#include "Payload.h"
#include <LUFA/Drivers/USB/USB.h>

static int buffIndex=0, getBuffIndex=0;

static command_t buff[200];



void AddCommand(int key, int modifier, int delay){

    command_t command = {
        .delay=delay,
        .key=key,
        .modifier=modifier};

    buff[buffIndex++]=command;
}

void AddText(char* t, int delay){

    for( int i = 0; t[i]!='\0'; i++){

        char c=t[i];

        command_t command = {
        .delay=delay,
        .key=0,
        .modifier=0};

        if(c>=65 && c<=90)
        {//uppercase letters
            command.key = c-61;
            command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
        }
        else if(c>=97 && c<=122)
        {//lowercase letters
            command.key = c-93;
        }
        else if(c>=49 && c<=57)
        {//numbers without 0
            command.key = c-19;
        }
        else{
            switch(c){
    
                case '0':
                command.key = HID_KEYBOARD_SC_0_AND_CLOSING_PARENTHESIS;
                break;  

                case ' ':
                command.key = HID_KEYBOARD_SC_SPACE;
                break;    

                case '"':
                command.key = HID_KEYBOARD_SC_APOSTROPHE_AND_QUOTE;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;   

                case '\t':
                command.key = HID_KEYBOARD_SC_TAB;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break; 

                case '\n':
                command.key = HID_KEYBOARD_SC_ENTER;
                break; 

                case ':':
                command.key = HID_KEYBOARD_SC_SEMICOLON_AND_COLON;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;  

                case ';':
                command.key = HID_KEYBOARD_SC_SEMICOLON_AND_COLON;
                break;  

                case '.':
                command.key = HID_KEYBOARD_SC_DOT_AND_GREATER_THAN_SIGN;
                break; 

                case ',':
                command.key = HID_KEYBOARD_SC_COMMA_AND_LESS_THAN_SIGN;
                break; 

                case '/':
                command.key = 	HID_KEYBOARD_SC_SLASH_AND_QUESTION_MARK;
                break; 

                case '=':
                command.key = 	HID_KEYBOARD_SC_EQUAL_AND_PLUS;
                break;

                case '?':
                command.key = 	HID_KEYBOARD_SC_SLASH_AND_QUESTION_MARK;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break; 

                case '!':
                command.key = HID_KEYBOARD_SC_1_AND_EXCLAMATION;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;   

                case '@':
                command.key = HID_KEYBOARD_SC_2_AND_AT;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;   

                case '#':
                command.key = HID_KEYBOARD_SC_3_AND_HASHMARK;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;    

                case '$':
                command.key = HID_KEYBOARD_SC_4_AND_DOLLAR;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;

                case '%':
                command.key = HID_KEYBOARD_SC_5_AND_PERCENTAGE;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;    

                case '^':
                command.key = HID_KEYBOARD_SC_6_AND_CARET;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;

                case '&':
                command.key = HID_KEYBOARD_SC_7_AND_AMPERSAND;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;    

                case '*':
                command.key = HID_KEYBOARD_SC_8_AND_ASTERISK;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;
                case '(':
                command.key = HID_KEYBOARD_SC_9_AND_OPENING_PARENTHESIS;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;    

                case ')':
                command.key = HID_KEYBOARD_SC_0_AND_CLOSING_PARENTHESIS;
                command.modifier=  HID_KEYBOARD_MODIFIER_LEFTSHIFT;
                break;
            }
        }
        buff[buffIndex++]=command;
    }

}

command_t GetCommand(){
    if( getBuffIndex < buffIndex){
        return buff[getBuffIndex++];
    }else {
        command_t command = {
            .delay=1000,
            .key=0,
            .modifier=0};
        return command;
    }
}