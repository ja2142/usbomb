#ifndef _LED_H_
#define _LED_H_

#include <avr/io.h>

#define LED_0 (1<<PF7)
#define LED_1 (1<<PF6)
#define LED_2 (1<<PF5)
#define LED_3 (1<<PF4)
#define LED_ALL LED_0|LED_1|LED_2|LED_3

#define LED_PORT PORTF
#define LED_DDR DDRF

#define leds_init() do{LED_DDR=0xF0;LED_PORT=0x00;}while(0);

#define leds_on(leds) (LED_PORT |= leds)
#define leds_off(leds) (LED_PORT &= ~(leds))
#define leds_switch(leds) (LED_PORT ^= leds)

#endif