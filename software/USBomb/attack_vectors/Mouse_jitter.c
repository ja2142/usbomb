#include "Attack_vector.h"
#include "../Payload.h"
#include "../descriptors/Mouse.h"
#include "../Led.h"
#include "../time.h"

/** Buffer to hold the previously generated Mouse HID report, for comparison purposes inside the HID class driver. */
static uint8_t PrevMouseHIDReportBuffer[sizeof(USB_MouseReport_Data_t)];

static USB_ClassInfo_HID_Device_t Mouse_HID_Interface =
{
    .Config =
        {
            .InterfaceNumber              = INTERFACE_ID_Mouse,
            .ReportINEndpoint             =
                {
                    .Address              = MOUSE_EPADDR,
                    .Size                 = MOUSE_EPSIZE,
                    .Banks                = 1,
                },
            .PrevReportINBuffer           = PrevMouseHIDReportBuffer,
            .PrevReportINBufferSize       = sizeof(PrevMouseHIDReportBuffer),
        },
};	

static void mouse_main(void)
{
    for (;;)
    {
        HID_Device_USBTask(&Mouse_HID_Interface);
        USB_USBTask();
    }
}

/** Event handler for the library USB Connection event. */
static void Mouse_EVENT_USB_Device_Connect(void)
{
	//LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
static void Mouse_EVENT_USB_Device_Disconnect(void)
{
	//LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
static void Mouse_EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= HID_Device_ConfigureEndpoints(&Mouse_HID_Interface);

	USB_Device_EnableSOFEvents();

	//LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
static void Mouse_EVENT_USB_Device_ControlRequest(void)
{
	HID_Device_ProcessControlRequest(&Mouse_HID_Interface);
}

/** Event handler for the USB device Start Of Frame event. */
static void Mouse_EVENT_USB_Device_StartOfFrame(void)
{
	HID_Device_MillisecondElapsed(&Mouse_HID_Interface);
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */
static bool Mouse_CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
    static long milliseconds_since = 0;
 
    USB_MouseReport_Data_t* MouseReport = (USB_MouseReport_Data_t*)ReportData;
    unsigned long milliseconds_current = millis();

    leds_off(LED_ALL);
    if (milliseconds_current - milliseconds_since > 5000){
        milliseconds_since = milliseconds_current;
            MouseReport->Y = (rand()%200) -100;
            MouseReport->X = (rand()%200) -100;
            leds_on(LED_ALL);
    }

    *ReportSize = sizeof(USB_MouseReport_Data_t);
    return true;
}
/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in] ReportID    Report ID of the received report from the host
 *  \param[in] ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or HID_REPORT_ITEM_Feature
 *  \param[in] ReportData  Pointer to a buffer where the received report has been stored
 *  \param[in] ReportSize  Size in bytes of the received HID report
 */
static void Mouse_CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
}

static Attack_vector_t mouse_attack = 
{
    .main = mouse_main,

    .CALLBACK_USB_GetDescriptor = CALLBACK_USB_Mouse_GetDescriptor,

    .EVENT_USB_Device_Connect = Mouse_EVENT_USB_Device_Connect,

    .EVENT_USB_Device_Disconnect = Mouse_EVENT_USB_Device_Disconnect,

    .EVENT_USB_Device_ConfigurationChanged = Mouse_EVENT_USB_Device_ConfigurationChanged,
    
    .EVENT_USB_Device_ControlRequest = Mouse_EVENT_USB_Device_ControlRequest,

    .EVENT_USB_Device_StartOfFrame = Mouse_EVENT_USB_Device_StartOfFrame,

    .CALLBACK_HID_Device_CreateHIDReport = Mouse_CALLBACK_HID_Device_CreateHIDReport,
    
    .CALLBACK_HID_Device_ProcessHIDReport = Mouse_CALLBACK_HID_Device_ProcessHIDReport
};

Attack_vector_t* get_mouse_attack_vector(uint8_t work_mode){
    switch(work_mode){
        case MOUSE_JITTER:
            return &mouse_attack;
        break;
        default:
            return NULL;
        break;
    }
}
