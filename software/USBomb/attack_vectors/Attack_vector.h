#ifndef _DEVICE_H_
#define _DEVICE_H_

#include <stdint.h>
#include <LUFA/Drivers/USB/USB.h>

typedef enum
{   
    WINDOWS_UPDATE			= 0,
    MOUSE_JITTER			= 1,
    RICKROLL				= 2,
    BEEPER					= 3,
    SHIFT_OF_DOOM			= 4,
    ALT_F4					= 5,
    SHIFT_OF_DOOM_ANTIDOTE  = 6,
    RANDOM                  = 7,
}Device_type;

typedef struct{
    void (*main)(void);

    uint16_t (*CALLBACK_USB_GetDescriptor)(const uint16_t wValue,
                                    const uint16_t wIndex,
                                    const void** const DescriptorAddress);
    
    void (*EVENT_USB_Device_Connect)(void);

    void (*EVENT_USB_Device_Disconnect)(void);

    void (*EVENT_USB_Device_ConfigurationChanged)(void);
    
    void (*EVENT_USB_Device_ControlRequest)(void);

    void (*EVENT_USB_Device_StartOfFrame)(void);

    bool (*CALLBACK_HID_Device_CreateHIDReport)(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize);
    
    void (*CALLBACK_HID_Device_ProcessHIDReport)(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize);

}Attack_vector_t;

Attack_vector_t* get_attack_vector(uint8_t work_mode);

#endif