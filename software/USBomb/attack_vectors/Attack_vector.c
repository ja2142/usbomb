#include "Attack_vector.h"
#include "Keyboard_attacks.h"
#include "Mouse_jitter.h"
#include "Beeper.h"
Attack_vector_t* get_attack_vector(uint8_t work_mode){
    Attack_vector_t* vector = get_keyboard_attack_vector(work_mode);
    if(vector){
        return vector;
    }
    vector = get_mouse_attack_vector(work_mode);
    if(vector){
        return vector;
    }
    vector = get_keyboard_audio_attack_vector(work_mode);
    if(vector){
        return vector;
    }
    return NULL;
}