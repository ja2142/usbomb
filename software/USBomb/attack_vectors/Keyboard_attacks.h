#ifndef _KEYBOARD_ATTACKS_H_
#define _KEYBOARD_ATTACKS_H_

#include "Attack_vector.h"

Attack_vector_t* get_keyboard_attack_vector(uint8_t work_mode);

#endif