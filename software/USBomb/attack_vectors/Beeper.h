#ifndef _BEEPER_H_
#define _BEEPER_H_

#include "Attack_vector.h"

Attack_vector_t* get_keyboard_audio_attack_vector(uint8_t work_mode);

#endif