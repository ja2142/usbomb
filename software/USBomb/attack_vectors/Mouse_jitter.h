#ifndef _MOUSE_JITTER_H_
#define _MOUSE_JITTER_H_

#include "Attack_vector.h"

Attack_vector_t* get_mouse_attack_vector(uint8_t work_mode);

#endif