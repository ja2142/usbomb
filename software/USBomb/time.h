#ifndef _TIME_H_
#define _TIME_H_

void timer_init(void);

unsigned long millis(void);

#endif