#ifndef _PAYLOAD_H_
#define _PAYLOAD_H_

	typedef struct{
        int key;
        int modifier;
        int delay;
    }command_t;

	/* Function Prototypes: */
       	void AddCommand(int key, int modifier, int delay);

        void AddText(char* text, int delay);

        command_t GetCommand(void);
        
#endif